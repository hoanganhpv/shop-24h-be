const express = require("express");
const { createAnOrderDetailOfOrder, getAllOrderDetailOfAnOrder, getAnOrderDetailById, updateOrderDetail, deleteOrderDetail } = require("../controllers/orderDetail.controller");
const { orderDetailMiddleware } = require("../middlewares/orderDetail.middleware");


const orderDetailRouter = express.Router();

// router create order detail of an order
orderDetailRouter.post("/orders/:orderId/details", orderDetailMiddleware, createAnOrderDetailOfOrder);

// router get all orderDetail of one customer: 
orderDetailRouter.get("/orders/:orderId/details", orderDetailMiddleware, getAllOrderDetailOfAnOrder);

// router get orderDetail by ID
orderDetailRouter.get("/details/:detailId", orderDetailMiddleware, getAnOrderDetailById);

// router update orderDetail by ID
orderDetailRouter.put("/details/:detailId", orderDetailMiddleware, updateOrderDetail);

// router delete orderDetail by ID
orderDetailRouter.delete("/orders/:orderId/details/:detailId", orderDetailMiddleware, deleteOrderDetail);

module.exports = {orderDetailRouter}