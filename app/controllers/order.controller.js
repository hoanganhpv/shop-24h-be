const mongoose = require("mongoose");
const orderModel = require("../models/order.model");
const customerModel = require("../models/customer.model");

// Create order of a customer
const createOrderOfCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer ID is invalid"
        });
    }
    // check orderDate
    if ( body.orderDate !== undefined && body.orderDate.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order Date is invalid"
        });
    }
    // check shippedDate
    if ( body.shippedDate !== undefined && body.shippedDate.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Shipped Date is invalid"
        });
    }
    // check note
    if ( body.note !== undefined && body.note.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order Date is invalid"
        });
    }
    // check cost
    if ( !Number.isInteger(body.cost) || body.cost < 0 ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Cost is invalid"
        });
    }

    // B3: Gọi model và xử lý CSDL
    let newOrder = {
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost
    }
    orderModel.create(newOrder, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            // Add order objectId to customer collection
            customerModel.findByIdAndUpdate(customerId, {
                $push: { orders: data._id }
            }, (err1, data1) => {
                if (err1) {
                    return res.status(500).json({
                        status: "Server Internal Error",
                        message: err1.message
                    });
                }
            })
            return res.status(201).json({
                status: "Create Order successful",
                data
            });
        }
    })
}

// Get all orders in database
const getAllOrdersDatabase = (req, res) => {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Gọi model và xử lý CSDL
    orderModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get All Orders in Database successful",
                data
            });
        }
    })
}

// // Get all orders of a customer
const getAllOrdersOfCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    customerModel.findById(customerId).populate("orders").exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get All Orders of Customer successful",
                data: data.orders
            });
        }
    })
}

// Get order detail by ID
const getOrderDetailById = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    orderModel.findById(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get Order Detail successful",
                data
            });
        }
    })
}

// Update order by ID
const updateOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order ID is invalid"
        });
    }
    // check if orderDate exist but not valid
    if ( body.orderDate !== undefined && body.orderDate.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order Date is invalid"
        });
    }
    // check if shippedDate exist but not valid
    if ( body.shippedDate !== undefined && body.shippedDate.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Shipped Date is invalid"
        });
    }
    // check if note exist but not valid
    if ( body.note !== undefined && body.note.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order Date is invalid"
        });
    }
    // check if cost exist but not valid
    if ( body.cost !== undefined && (!Number.isInteger(body.cost) || body.cost < 0) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Cost is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let updateOrder = {
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost
    }
    orderModel.findByIdAndUpdate(orderId, updateOrder, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Update Order Detail successful",
                data
            });
        }
    })
}

// Delete order by ID
const deleteOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order ID is invalid"
        });
    }

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    orderModel.findByIdAndDelete(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err
            });
        }
        else {
            customerModel.findByIdAndUpdate(customerId, {
                $pull: {orders: orderId}
            }, (err1, data1) => {
                if (err1) {
                    return res.status(500).json({
                        status: "Server Internal Error",
                        message: err1.message
                    });
                }
            })
            return res.status(204).json();
        }
    })
}

module.exports = { createOrderOfCustomer, getAllOrdersDatabase, getAllOrdersOfCustomer, getOrderDetailById, updateOrder, deleteOrder }