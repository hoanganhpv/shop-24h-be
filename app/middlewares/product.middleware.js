const productMiddleware = (req, res, next) => {
    console.log("METHOD: " + req.method);
    next();
}

module.exports = { productMiddleware };